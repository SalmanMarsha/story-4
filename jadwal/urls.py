from django.urls import path

from . import views

app_name = "jadwal"

urlpatterns = [    
    path ('', views.index, name="index"),
    path ('form/', views.jadwal, name='form'),
    path ('detail/', views.semua, name="semua"),
    path ('detail/<int:index>/', views.detail, name="detail"),
]