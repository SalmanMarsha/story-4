from django.db import models


# Create your models here.
class Jadwal(models.Model):
    pilihan_semester= (
        ('Genap', "Genap"),
        ('Gasal', 'Gasal')
    )
    nama = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    sks = models.PositiveSmallIntegerField(default=1)
    semester = models.CharField(max_length=100, choices=pilihan_semester, default="Gasal")
    tahun = models.CharField(max_length=100)
    ruang = models.CharField(max_length=100)
    deskripsi = models.TextField(max_length=100)

    def __str__(self):
        return f'{self.nama}'

class Tugas(models.Model):
    nama_tugas = models.CharField(max_length=100)
    deadline_tugas =models.DateField()
    nama_matkul =models.ForeignKey(Jadwal, on_delete = models.CASCADE)

