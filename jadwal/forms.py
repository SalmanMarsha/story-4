from django import forms
from .models import Jadwal

class madwal(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = "__all__"
        labels = {
            'nama'      : "Nama Jadwal",
            'dosen'      : "Nama Dosen",
            'sks'       : "Jumlah SKS",
            'semester'  : "Semester",
            'tahun'     : "Tahun Ajar",
            'ruang'     : 'Ruang',
            'deskripsi' : 'Deskripsi Mata Kuliah',
        }
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        }
        )
