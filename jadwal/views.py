from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import madwal
from .models import Jadwal, Tugas

# Create your views here.
def index (request):
    return render(request,"mindex.html")

def jadwal(request):
    if request.method == 'POST':
        form = madwal(request.POST)
        if form.is_valid():
            form.save()
            return redirect('jadwal:form')
    else:
        messages.warning(request,"Jadwal gagal ditambahkan")
        form = madwal()
        return render(request, "form.html", {"form" : form,})

def semua(request):
    semua = Jadwal.objects.all()
    if request.method == "POST":
        data = Jadwal.objects.get(pk=request.POST["idhapus"])
        data.delete()
    return render(request, "semua.html", {"semua":semua})


def detail(request, index):
    detail = Jadwal.objects.get(pk=index)
    matkul = Tugas.objects.filter(nama_matkul=detail)
    return render(request,'detail.html', {"detail":detail, "matkul":matkul})

