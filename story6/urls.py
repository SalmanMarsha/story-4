from django.urls import path

from . import views

app_name = "story6"

urlpatterns = [    
    path ('', views.index_kegiatan, name="index"),
    path ('form/', views.kegiatan, name='form'),
    path ('detail/', views.list_kegiatan, name="semua"),
    path ('detail/<int:id>/', views.detail_kegiatan, name="detail"),
    path ('detail/delete/<int:id>/', views.delete_peserta, name="delete")
]