from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import IkutKegiatan, JoinKegiatan
from .models import Kegiatan, Peserta
# Create your views here.
def index_kegiatan (request):
    return render(request,"index_kegiatan.html")

def kegiatan(request):
    if request.method == 'POST':
        form = IkutKegiatan(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect('story6:form')
    else:
        form = IkutKegiatan()
        return render(request, "form_kegiatan.html", {"form" : form})

def list_kegiatan(request):
    semua_kegiatan = Kegiatan.objects.all()
    semua_peserta = Peserta.objects.all()
    if request.method == "POST":
        return render(request, "form_peserta.html")
    return render(request, "list_kegiatan.html", {"semua":semua_kegiatan, "peserta":semua_peserta})


def detail_kegiatan(request, id=0):
    if request.method == "POST": 
        peserta = JoinKegiatan(request.POST or None)
        if peserta.is_valid():
            penumpang = Peserta(ikut_kegiatan=Kegiatan.objects.get(pk = id), nama_peserta=peserta.data['nama_peserta'])
            penumpang.save() 
        return redirect('/kegiatan/detail')
    peserta = JoinKegiatan()
    return render(request, "form_peserta.html",{'peserta':peserta})

def delete_peserta(request, id):
    peeps = Peserta.objects.get(pk= id)
    peeps.delete()
    return redirect('/kegiatan/detail')

    
