from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=100)
    deskripsi_kegiatan = models.TextField()

    def __str__(self):
        return self.nama_kegiatan

class Peserta(models.Model):
    nama_peserta = models.CharField(max_length=100)
    ikut_kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.nama_peserta