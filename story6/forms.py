from django import forms
from .models import Kegiatan, Peserta

class IkutKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = "__all__"
        labels = {
            'nama_kegiatan'     : "Nama Kegiatan",
            'deskripsi_kegiatan': "Deskripsi",
            
        }
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        }
        )

class JoinKegiatan(forms.ModelForm):
    class Meta:
        model = Peserta
        fields =["nama_peserta"]
        labels ={
            'nama_peserta' : "Nama Peserta",
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        }
        )
