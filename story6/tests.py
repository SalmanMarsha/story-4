from django.test import TestCase, Client
from django.urls import resolve
from .views import index_kegiatan, kegiatan, list_kegiatan, detail_kegiatan
from .models import Kegiatan, Peserta
from .apps import Story6Config

# Create your tests here.
class TestRouting(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get('/kegiatan/')
		self.assertEqual(response.status_code, 200)
	def test_event2_url_is_exist(self):
		response = Client().get('/kegiatan/form/')
		self.assertEqual(response.status_code, 200)
	def test_event3_url_is_exist(self):
		response = Client().get('/kegiatan/detail/')
		self.assertEqual(response.status_code, 200)
	def test_event_using_template(self):
		response = Client().get('/kegiatan/')
		self.assertTemplateUsed(response, 'index_kegiatan.html')
	def test_event2_using_template(self):
		response = Client().get('/kegiatan/form/')
		self.assertTemplateUsed(response, 'form_kegiatan.html')
	def test_event3_using_template(self):
		response = Client().get('/kegiatan/detail/')
		self.assertTemplateUsed(response, 'list_kegiatan.html')
	def test_event4_using_template(self):
		new_activity = Kegiatan.objects.create(nama_kegiatan="Ultah Salman", deskripsi_kegiatan="Ahmadun")
		getid = new_activity.id
		response = Client().get('/kegiatan/detail/' + str(getid) + "/")
		self.assertTemplateUsed(response, 'form_peserta.html')


class TestFunc(TestCase):
	def test_views1_func(self):
		found = resolve('/kegiatan/')
		self.assertEqual(found.func, index_kegiatan)
	
	def test_event_func(self):
		found = resolve('/kegiatan/form/')
		self.assertEqual(found.func, kegiatan)
	
	def test_views3_func(self):
		found = resolve('/kegiatan/detail/')
		self.assertEqual(found.func, list_kegiatan)
	
class TestModels(TestCase):
	def test_model_can_create(self):
		new_activity = Kegiatan.objects.create(nama_kegiatan="Ultah Salman", deskripsi_kegiatan="Ahmadun")
		counting_all_available_todo = Kegiatan.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)

	def test_model2_can_create(self):
		activity = Kegiatan.objects.create(nama_kegiatan="Ultah Salman", deskripsi_kegiatan="Ahmadun")
		peserta = Peserta.objects.create(nama_peserta="Ultah Salman", ikut_kegiatan=activity)
		counting_all_available_todo = Peserta.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)
	
	def test_model_can_print(self):
		kegiatan = Kegiatan.objects.create(nama_kegiatan="Ultah Salman", deskripsi_kegiatan="Ahmadun")
		self.assertEqual(kegiatan.__str__(), "Ultah Salman")
	
	def test_model2_can_print(self):
		activity = Kegiatan.objects.create(nama_kegiatan="Ultah Salman", deskripsi_kegiatan="Ahmadun")
		peserta = Peserta.objects.create(nama_peserta="Salman", ikut_kegiatan=activity)
		self.assertEqual(peserta.__str__(), "Salman")

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story6Config.name, "story6")

class TestDelete(TestCase):
	def test_peserta_delete(self):
		acara = Kegiatan.objects.create(nama_kegiatan="Anniv luthfi", deskripsi_kegiatan="Acara")
		orang = Peserta.objects.create(nama_peserta="Luthfi", ikut_kegiatan=acara)
		orang.delete()
		self.assertEqual(Peserta.objects.all().count(), 0)