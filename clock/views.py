from django.shortcuts import render
from datetime import timedelta
import datetime
 

def time(request, hour=0):
    currentDT = datetime.datetime.now() + timedelta(hours=hour) 
    hour = currentDT.hour
    minute = currentDT.minute
    second = currentDT.second
    return render(request, "time.html", {"hour": hour, 'minute': minute, 'second':second})