from django.urls import path

from . import views

urlpatterns = [    
    path('', views.time),
    path('<int:hour>/', views.time)
]